Pod::Spec.new do |s|
  s.name         = "Eli"
  s.version      = "0.0.1"
  s.summary      = "A short description of Eli."
  s.description  = "this is a simple function"
  s.homepage     = "http://EXAMPLE/Eli"
  s.license      = "MIT"
  s.author             = { "eliakorkmaz" => "emrahkrkmz1@gmail.com" }
  s.platform     = :ios
  s.source       = { :git => "http://EXAMPLE/Eli.git", :tag => "#{s.version}" }
  s.source_files  = "Classes", "Classes/**/*.{h,m}"
  s.exclude_files = "Classes/Exclude"
end
