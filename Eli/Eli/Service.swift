//
//  Service.swift
//  Eli
//
//  Created by Emrah Korkmaz on 10/24/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import Foundation


public class Service{
    
    private init() {}
    
    public static func doSomething() -> String {
        return "--"
    }
    
}
